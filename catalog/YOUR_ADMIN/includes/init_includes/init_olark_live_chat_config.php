<?php
  if (!defined('IS_ADMIN_FLAG')) {
    die('Illegal Access');
  }

$zencart_com_plugin_id = 1498; // from zencart.com plugins - Leave Zero not to check
$current_version = '1.1.3';
  // add upgrade script
  $olark_live_chat_version = (defined('OLARK_LIVE_CHAT_VERSION') ? OLARK_LIVE_CHAT_VERSION : 'new');

if($olark_live_chat_version != 'new') {
    $config = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key= 'OLARK_LIVE_CHAT_VERSION'");
    $configuration_group_id = $config->fields['configuration_group_id'];
}

  while ($olark_live_chat_version != $current_version) {
    switch($olark_live_chat_version) {
      case 'new':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_0_0.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_0_0.php');
          $olark_live_chat_version = '1.0.0';          
					break;
        } else {
         	break 2;
				}
      case '1.0.0':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_0.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_0.php');
          $olark_live_chat_version = '1.1.0';          
					break;
        } else {
         	break 2;
				}
      case '1.1.0':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_1.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_1.php');
          $olark_live_chat_version = '1.1.1';          
					break;
        } else {
         	break 2;
				}
      case '1.1.1':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_2.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_2.php');
          $olark_live_chat_version = '1.1.2';          
					break;
        } else {
         	break 2;
				}
      case '1.1.2':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_3.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_3.php');
          $olark_live_chat_version = '1.1.3';
          break;
        } else {
          break 2;
        }
      case '1.1.3':
        // perform upgrade
        if (file_exists(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_4.php')) {
          include_once(DIR_WS_INCLUDES . 'installers/olark_live_chat/1_1_4.php');
          $olark_live_chat_version = '1.1.4';
          break;
        } else {
          break 2;
        }
      default:
        $olark_live_chat_version = $current_version;
        // break all the loops
        break 2;
    }
  }


// Version Checking
if ($zencart_com_plugin_id != 0) {
    if (isset($_GET['gID']) && $_GET['gID'] == $configuration_group_id) {
        $new_version_details = plugin_version_check_for_updates($zencart_com_plugin_id, $current_version);
        if ($new_version_details != FALSE) {
            $messageStack->add("Version " . $new_version_details['latest_plugin_version'] . " of " . $new_version_details['title'] . ' is available at <a href="' . $new_version_details['link'] . '" target="_blank">[Details]</a>', 'caution');
        }
    }
}

if (!function_exists('plugin_version_check_for_updates')) {

    function plugin_version_check_for_updates($fileid = 0, $version_string_to_check = '') {
        if ($fileid == 0) {
            return FALSE;
        }
        $new_version_available = FALSE;
        $lookup_index = 0;
        $url = 'http://www.zen-cart.com/downloads.php?do=versioncheck' . '&id=' . (int) $fileid;
        $data = json_decode(file_get_contents($url), true);
        // compare versions
        if (version_compare($data[$lookup_index]['latest_plugin_version'], $version_string_to_check) > 0) {
            $new_version_available = TRUE;
        }
        // check whether present ZC version is compatible with the latest available plugin version
        if (!in_array('v' . PROJECT_VERSION_MAJOR . '.' . PROJECT_VERSION_MINOR, $data[$lookup_index]['zcversions'])) {
            $new_version_available = FALSE;
        }
        if ($version_string_to_check == true) {
            return $data[$lookup_index];
        } else {
            return FALSE;
        }
    }

}